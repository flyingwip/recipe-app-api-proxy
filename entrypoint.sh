#!/bin/sh

# if any of the lines fail then return a failure and print error to screen
set -e

# The envsubst program substitutes the values of environment variables.
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
# start the NGINX service
# default NGINX processes run as a background deamon service 
# but this is not recommended in a docker image.
# in a docker container the primary application should run in the foreground
nginx -g 'daemon off;'