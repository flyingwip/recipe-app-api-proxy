server {
  listen ${LISTEN_PORT};

  location /static {
    # we will map a volume to our running container
    # that contains all of the static files
    # there the prefix `vol`   
    alias /vol/static;
  }

  location / {
    uwsgi_pass           ${APP_HOST}:${APP_PORT};
    include              /etc/nginx/uwsgi_params;
    client_max_body_size 10M;
  }
}
