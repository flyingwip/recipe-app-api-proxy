# This image is used because the most popular image 
# the service is running with the root user by default
# and it is not best pracice to be running containers in production

# Best practice : docker container is running as the least-privileged user
FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="maintainer@londonappdev.com"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

USER root

# the -p flag tells the mkdir command to create all subdirectories (if not exist)
RUN mkdir -p /vol/static
# set permissions see https://chmod-calculator.com/
# this dir will be used to serve through NGINX proxy
RUN chmod 755 /vol/static
# create empty file
RUN touch /etc/nginx/conf.d/default.conf
# change ownership [user:group]
# why we do this is because the nginx user has not the permissions to create the file
# so the root user creates the file and then change the ownership to nginx user and group.
# the nginx user will be running the entrypoint.sh 
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /entrypoint.sh
# make file executable
RUN chmod +x /entrypoint.sh
# swap user to nginx
USER nginx
# set the default command
CMD ["/entrypoint.sh"]
